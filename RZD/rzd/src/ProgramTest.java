import org.junit.Test;
import rzd.blogic.ClientLogic;
import rzd.data.ClientEntity;
import rzd.servicelayer.LoginServ;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ProgramTest {
    @Test
    public void testAddNewPlayer() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        LoginServ loginServ = new LoginServ();

        String newLogin = "test";
        String newPassword = "test";
        if (!loginServ.loginClient(newLogin, newPassword)) {
            ClientLogic.registerClient(newLogin, newPassword, entityManager);

            if (ClientLogic.loginClient(newLogin, newPassword, entityManager) != null) {
                System.out.println("Пользователь добавлен успешно!");
            } else {
                System.err.println("Пользователь не добавлен!");
            }
        } else {
            System.err.println("Пользователь уже добавлен!");
        }
    }

    @Test
    public void testIsAdmin () throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        String newLogin = "test";
        String newPassword = "test";
        ClientEntity clientEntity = ClientLogic.registerClient(newLogin, newPassword, entityManager);

        if (clientEntity.isClientIsadmin()){
            System.out.println("Свойство \"администратор\" работает успешно!");
        } else  {
            System.err.println("Свойство \"администратор\" не работает!");
        }
    }
}