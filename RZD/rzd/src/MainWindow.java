import rzd.servicelayer.LoginServ;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class MainWindow extends javax.swing.JFrame {
    private  LoginServ           loginServ;

    private void backButtonActionPerformed() {
        this.dispose();
        LoginWindow loginWindow = new LoginWindow();
        loginWindow.setVisible(true);
    }

    private void ticketsButtonActionPerformed() {
        this.dispose();
        TicketsWindow ticketsWindow = new TicketsWindow(loginServ);
        ticketsWindow.setVisible(true);
    }

    public MainWindow(LoginServ loginServ) {
        setTitle("Main window");
        this.loginServ = loginServ;
        initComponents();
    }

    private void searchButtonActionPerformed() {
        SearchTicketWindow Select = new SearchTicketWindow(loginServ);
        Select.setVisible(true);
        this.dispose();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    // Generated using JFormDesigner Evaluation license - rr kk
    private void initComponents() {
        helloLabel = new JLabel();
        backButton = new JButton();
        ticketPanel = new JPanel();
        searchButton = new JButton();
        ticketsButton = new JButton();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container contentPane = getContentPane();

        //---- helloLabel ----
        helloLabel.setText("\u0414\u043e\u0431\u0440\u043e \u043f\u043e\u0436\u0430\u043b\u043e\u0432\u0430\u0442\u044c! \u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0438\u043d\u0442\u0435\u0440\u0435\u0441\u0443\u044e\u0449\u0438\u0439 \u0412\u0430\u0441 \u043f\u0443\u043d\u043a\u0442 \u043c\u0435\u043d\u044e");

        //---- backButton ----
        backButton.setText("\u041d\u0430\u0437\u0430\u0434");
        backButton.addActionListener(e -> backButtonActionPerformed());

        //======== ticketPanel ========
        {
            ticketPanel.setBorder(new TitledBorder("\u0411\u0438\u043b\u0435\u0442\u044b"));

            ticketPanel.setLayout(null);

            //---- searchButton ----
            searchButton.setText("\u041f\u043e\u0438\u0441\u043a \u0431\u0438\u043b\u0435\u0442\u043e\u0432");
            searchButton.addActionListener(e -> searchButtonActionPerformed());
            ticketPanel.add(searchButton);
            searchButton.setBounds(new Rectangle(new Point(25, 30), searchButton.getPreferredSize()));

            //---- ticketsButton ----
            ticketsButton.setText("\u0423\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u0431\u0438\u043b\u0435\u0442\u0430\u043c\u0438");
            ticketsButton.addActionListener(e -> ticketsButtonActionPerformed());
            ticketPanel.add(ticketsButton);
            ticketsButton.setBounds(new Rectangle(new Point(185, 30), ticketsButton.getPreferredSize()));

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < ticketPanel.getComponentCount(); i++) {
                    Rectangle bounds = ticketPanel.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = ticketPanel.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                ticketPanel.setMinimumSize(preferredSize);
                ticketPanel.setPreferredSize(preferredSize);
            }
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addComponent(backButton, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap())
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addComponent(ticketPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(helloLabel))
                            .addGap(65, 65, 65))))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addComponent(helloLabel, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(ticketPanel, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(backButton)
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - rr kk
    private JLabel helloLabel;
    private JButton backButton;
    private JPanel ticketPanel;
    private JButton searchButton;
    private JButton ticketsButton;
    // End of variables declaration//GEN-END:variables
}
