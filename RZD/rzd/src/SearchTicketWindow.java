import javax.swing.border.*;
import rzd.servicelayer.LoginServ;
import rzd.utils.ModalDialog;

import javax.swing.*;
import java.awt.*;
import java.util.Date;
import java.util.List;

public class SearchTicketWindow extends javax.swing.JFrame {
    private  LoginServ       loginServ;

    private void countyFromComboBoxActionPerformed() {
        fillLocal(true);
    }

    private void fillLocal(boolean isFrom) {
        JComboBox countryComboBox = countryFromComboBox;
        JComboBox localComboBox = localFromComboBox;
        if (!isFrom) {
            countryComboBox = countryToComboBox;
            localComboBox = localToComboBox;
        }
            if (countryComboBox.getSelectedIndex() > -1) {
                List<String> names = loginServ.getLocalCountry(countryComboBox.getSelectedIndex());
                localComboBox.removeAllItems();
                names.forEach(localComboBox::addItem);
        }
    }

    private void fillStation(boolean isFrom) {
        JComboBox countryComboBox = countryFromComboBox;
        JComboBox localComboBox = localFromComboBox;
        JComboBox stationComboBox = stationFromComboBox;
        if (!isFrom) {
            countryComboBox = countryToComboBox;
            localComboBox = localToComboBox;
            stationComboBox = stationToComboBox;
        }
        if (countryComboBox.getSelectedIndex() > -1 && localComboBox.getSelectedIndex() > -1) {
            List<String> names = loginServ.getStationLocal(countryComboBox.getSelectedIndex(), localComboBox.getSelectedIndex());
            stationComboBox.removeAllItems();
            names.forEach(stationComboBox::addItem);
        }
    }

    private void countyToComboBoxActionPerformed() {
        fillLocal(false);
    }

    private void searchButtonActionPerformed() {
        try {
            String day = dayField.getText();
            String month = monthField.getText();
            String year = yearField.getText();
            Date date;
            if (day.equals("") || month.equals("") || year.equals("")) {
                ModalDialog.showError(this, "Wrong date");
                return;
            } else {
                int dayI = Integer.valueOf(day);
                int monthI = Integer.valueOf(month);
                int yearI = Integer.valueOf(year);
                if (dayI < 1 || dayI > 31 || monthI < 1 || monthI > 12 || yearI < 1) {
                    ModalDialog.showError(this, "Wrong date");
                    return;
                }
                date = new Date(yearI, monthI, dayI);
            }

            if (countryFromComboBox.getSelectedIndex() < 0 || countryToComboBox.getSelectedIndex() < 0) {
                ModalDialog.showError(this, "Wrong country");
                return;
            }

            Object[][] objects  = loginServ.search(countryFromComboBox.getSelectedIndex(),countryToComboBox.getSelectedIndex(),
                    localFromComboBox.getSelectedIndex(),localToComboBox.getSelectedIndex(),stationFromComboBox.getSelectedIndex(),
                    stationToComboBox.getSelectedIndex(),date);

            this.dispose();
            TripWindow tripWindow = new TripWindow(loginServ, objects);
            tripWindow.setVisible(true);

        } catch (NumberFormatException e) {
            ModalDialog.showError(this, "Wrong date");
        }
    }

    private void backbuttonActionPerformed() {
        this.dispose();
        MainWindow mainWindow = new MainWindow(loginServ);
        mainWindow.setVisible(true);
    }

    private void localFromComboBoxActionPerformed() {
        fillStation(true);
    }

    private void localToComboBoxActionPerformed() {
        fillStation(false);
    }

    public SearchTicketWindow(LoginServ loginServ) {
        setTitle("Search tickets window");
        this.loginServ = loginServ;
        initComponents();
        List<String> countries = loginServ.getAllCountries();
        countryFromComboBox.removeAllItems();
        countries.forEach(countryFromComboBox::addItem);
        countryToComboBox.removeAllItems();
        countries.forEach(countryToComboBox::addItem);
    }

    private JTextField dayField;
    private JTextField monthField;
    private JTextField yearField;
    private JComboBox stationFromComboBox;
    private JComboBox localFromComboBox;
    private JComboBox countryFromComboBox;
    private JComboBox stationToComboBox;
    private JComboBox localToComboBox;
    private JComboBox countryToComboBox;

        private void initComponents() {
        JLabel dateLabel = new JLabel();
        JButton searchButton = new JButton();
        dayField = new JTextField();
        monthField = new JTextField();
        yearField = new JTextField();
        JButton backButton = new JButton();
        JPanel fromPanel = new JPanel();
        JLabel countryFromLabel = new JLabel();
        JLabel localFromLabel = new JLabel();
        JLabel stationFromLabel = new JLabel();
        stationFromComboBox = new JComboBox();
        localFromComboBox = new JComboBox();
        countryFromComboBox = new JComboBox();
        JPanel toPanel = new JPanel();
        JLabel countryToLabel = new JLabel();
        JLabel localToLabel = new JLabel();
        JLabel stationToLabel = new JLabel();
        stationToComboBox = new JComboBox();
        localToComboBox = new JComboBox();
        countryToComboBox = new JComboBox();
        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container contentPane = getContentPane();

        //---- dateLabel ----
        dateLabel.setText("\u0414\u0430\u0442\u0430");

        //---- searchButton ----
        searchButton.setText("\u041f\u043e\u0438\u0441\u043a");
        searchButton.addActionListener(e -> searchButtonActionPerformed());

        //---- backButton ----
        backButton.setText("\u041d\u0430\u0437\u0430\u0434");
        backButton.addActionListener(e -> backbuttonActionPerformed());

        //======== fromPanel ========
        {
            fromPanel.setBorder(new TitledBorder("\u041f\u0443\u043d\u043a\u0442 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f"));

            //---- coutryFromLabel ----
            countryFromLabel.setText("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0441\u0442\u0440\u0430\u043d\u0443 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f");

            //---- localFromLabel ----
            localFromLabel.setText("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u043d\u0430\u0441\u0435\u043b\u0435\u043d\u043d\u044b\u0439 \u043f\u0443\u043d\u043a\u0442 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f");

            //---- stationFromLabel ----
            stationFromLabel.setText("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0432\u043e\u043a\u0437\u0430\u043b \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f");

            //---- localFromComboBox ----
            localFromComboBox.addActionListener(e -> localFromComboBoxActionPerformed());

            //---- countryFromComboBox ----
            countryFromComboBox.addActionListener(e -> countyFromComboBoxActionPerformed());

            GroupLayout fromPanelLayout = new GroupLayout(fromPanel);
            fromPanel.setLayout(fromPanelLayout);
            fromPanelLayout.setHorizontalGroup(
                fromPanelLayout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, fromPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(fromPanelLayout.createParallelGroup()
                            .addComponent(countryFromLabel)
                            .addComponent(localFromLabel)
                            .addComponent(stationFromLabel))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(fromPanelLayout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, fromPanelLayout.createParallelGroup()
                                .addComponent(localFromComboBox, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
                                .addComponent(countryFromComboBox, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE))
                            .addComponent(stationFromComboBox, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
            );
            fromPanelLayout.setVerticalGroup(
                fromPanelLayout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, fromPanelLayout.createSequentialGroup()
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(fromPanelLayout.createParallelGroup()
                            .addComponent(countryFromLabel)
                            .addComponent(countryFromComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(fromPanelLayout.createParallelGroup()
                            .addComponent(localFromLabel)
                            .addComponent(localFromComboBox, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                        .addGroup(fromPanelLayout.createParallelGroup()
                            .addGroup(fromPanelLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(stationFromLabel))
                            .addGroup(fromPanelLayout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(stationFromComboBox, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
            );
        }

        //======== toPanel ========
        {
            toPanel.setBorder(new TitledBorder("\u041f\u0443\u043d\u043a\u0442 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f"));

            //---- coutryToLabel ----
            countryToLabel.setText("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0441\u0442\u0440\u0430\u043d\u0443 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f");

            //---- localToLabel ----
            localToLabel.setText("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u043d\u0430\u0441\u0435\u043b\u0435\u043d\u043d\u044b\u0439 \u043f\u0443\u043d\u043a\u0442 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f");

            //---- stationToLabel ----
            stationToLabel.setText("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0432\u043e\u043a\u0437\u0430\u043b \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f");

            //---- localToComboBox ----
            localToComboBox.addActionListener(e -> localToComboBoxActionPerformed());

            //---- countryToComboBox ----
            countryToComboBox.addActionListener(e -> countyToComboBoxActionPerformed());

            GroupLayout toPanelLayout = new GroupLayout(toPanel);
            toPanel.setLayout(toPanelLayout);
            toPanelLayout.setHorizontalGroup(
                toPanelLayout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, toPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(toPanelLayout.createParallelGroup()
                            .addComponent(countryToLabel)
                            .addComponent(localToLabel)
                            .addComponent(stationToLabel))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 208, Short.MAX_VALUE)
                        .addGroup(toPanelLayout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, toPanelLayout.createParallelGroup()
                                .addComponent(localToComboBox, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
                                .addComponent(countryToComboBox, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE))
                            .addComponent(stationToComboBox, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
            );
            toPanelLayout.setVerticalGroup(
                toPanelLayout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, toPanelLayout.createSequentialGroup()
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(toPanelLayout.createParallelGroup()
                            .addComponent(countryToLabel)
                            .addComponent(countryToComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(toPanelLayout.createParallelGroup()
                            .addComponent(localToLabel)
                            .addComponent(localToComboBox, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                        .addGroup(toPanelLayout.createParallelGroup()
                            .addGroup(toPanelLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(stationToLabel))
                            .addGroup(toPanelLayout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(stationToComboBox, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
            );
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addComponent(dateLabel)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dayField, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(monthField, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(yearField)
                            .addGap(32, 32, 32))
                        .addComponent(fromPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(searchButton, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 273, Short.MAX_VALUE)
                            .addComponent(backButton)
                            .addGap(19, 19, 19))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addComponent(toPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addContainerGap())))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(fromPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                    .addComponent(toPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(dateLabel)
                        .addComponent(dayField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(monthField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(yearField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(78, 78, 78)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(searchButton)
                        .addComponent(backButton))
                    .addGap(25, 25, 25))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
}


