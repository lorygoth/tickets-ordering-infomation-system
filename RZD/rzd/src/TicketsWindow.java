import rzd.servicelayer.LoginServ;
import rzd.utils.ModalDialog;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class TicketsWindow extends JFrame {
    private LoginServ loginServ;

    public TicketsWindow(LoginServ loginServ) {
        setTitle("Ticket window");
        this.loginServ = loginServ;
        initComponents();
        if (!loginServ.isAdmin()) {
            okButton.setEnabled(false);
            fillTable();
        }
        fillTable();
    }

    private void fillTable() {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        Object[][] objects = loginServ.getTickets();
        for (Object[] object : objects) {
            model.addRow(object);
        }
    }

    private void backButtonActionPerformed() {
        this.dispose();
        MainWindow mainWindow  = new MainWindow(loginServ);
        mainWindow.setVisible(true);
    }

    private void cancelButtonActionPerformed() {
        if (table.getSelectedRow() >= 0) {
            ModalDialog.showComplete(this, "Бронирование отменено");
            loginServ.cancelTicket(table.getSelectedRow());
            fillTable();
        }
    }

    private void okButtonActionPerformed() {
        if (table.getSelectedRow() >= 0) {
            ModalDialog.showComplete(this, "Бронирование подтверждено");
            loginServ.okTicket(table.getSelectedRow());
            fillTable();
        }
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - rr kk
        JScrollPane scrollPane1 = new JScrollPane();
        table = new JTable();
        cancelButton = new JButton();
        backButton = new JButton();
        okButton = new JButton();

        //======== this ========
        Container contentPane = getContentPane();

        //======== scrollPane1 ========
        {

            //---- table ----
            table.setModel(new DefaultTableModel(
                new Object[][] {
                    {null, null},
                },
                new String[] {
                    "ID", "Status"
                }
            ) {
                Class<?>[] columnTypes = new Class<?>[] {
                    Integer.class, String.class
                };
                boolean[] columnEditable = new boolean[] {
                    false, true
                };
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                    return columnTypes[columnIndex];
                }
                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return columnEditable[columnIndex];
                }
            });
            scrollPane1.setViewportView(table);
        }

        //---- cancelButton ----
        cancelButton.setText("\u041e\u0442\u043c\u0435\u043d\u0438\u0442\u044c");
        cancelButton.addActionListener(e -> cancelButtonActionPerformed());

        //---- backButton ----
        backButton.setText("\u041d\u0430\u0437\u0430\u0434");
        backButton.addActionListener(e -> backButtonActionPerformed());

        //---- okButton ----
        okButton.setText("\u041f\u043e\u0434\u0442\u0432\u0435\u0440\u0434\u0438\u0442\u044c");
        okButton.addActionListener(e -> okButtonActionPerformed());

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 589, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(10, Short.MAX_VALUE))
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addGap(0, 12, Short.MAX_VALUE)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                .addComponent(cancelButton, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(okButton, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                                .addGap(275, 275, 275))
                        .addComponent(backButton, GroupLayout.Alignment.TRAILING)))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 362, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(cancelButton)
                        .addComponent(okButton))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(backButton)
                    .addContainerGap(14, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    private JTable table;
    private JButton cancelButton;
    private JButton backButton;
    private JButton okButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
