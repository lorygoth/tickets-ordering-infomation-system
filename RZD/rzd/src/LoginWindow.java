
import rzd.servicelayer.LoginServ;
import rzd.utils.ModalDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;

public class LoginWindow extends JFrame implements KeyListener {
private LoginServ loginServ;

    private void registerButtonActionPerformed() {
        if (checkFields ()) {
            if (loginServ.registerUser(loginField.getText(), passwordField.getText())) {
                MainWindow mainWindow = new MainWindow(loginServ);
                mainWindow.setVisible(true);
                this.dispose();
            } else {
                ModalDialog.showError(this, "Такой пользователь уже есть");
            }
        }
    }

    private boolean checkFields () {
        if (loginField.getText().trim().equals("") || passwordField.getText().trim().equals("")) {
            ModalDialog.showError(this, "Пустые поля");
            return false;
        }
        return true;
    }

    private void enterActionPerformed() {
        if (checkFields()) {
            if (loginServ.loginClient(loginField.getText(), passwordField.getText())) {
                MainWindow mainWindow = new MainWindow(loginServ);
                mainWindow.setVisible(true);
                this.dispose();
            } else {
                ModalDialog.showError(this, "Неверный пароль");
            }
        }
    }


    public LoginWindow() {
        setTitle("Login window");
        initComponents();
        try {
            loginServ = new LoginServ();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        passwordField.addKeyListener(this); //Что бы заходить по enter
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    // Generated using JFormDesigner Evaluation license - d fd
    private void initComponents() {
        passwordField = new JPasswordField();
        loginField = new JTextField();
        registerButton = new JButton();
        loginButton = new JButton();
        label1 = new JLabel();
        label2 = new JLabel();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container contentPane = getContentPane();

        //---- passwordField ----
        passwordField.setHorizontalAlignment(SwingConstants.CENTER);

        //---- loginField ----
        loginField.setHorizontalAlignment(SwingConstants.CENTER);
        loginField.setToolTipText("\u041b\u043e\u0433\u0438\u043d");

        //---- registerButton ----
        registerButton.setText("\u0420\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044f");
        registerButton.setContentAreaFilled(false);
        registerButton.setFocusPainted(false);
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerButtonActionPerformed();
            }
        });

        //---- loginButton ----
        loginButton.setText("\u0412\u0445\u043e\u0434");
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                enterActionPerformed();
            }
        });

        //---- label1 ----
        label1.setText("\u041b\u043e\u0433\u0438\u043d");

        //---- label2 ----
        label2.setText("\u041f\u0430\u0440\u043e\u043b\u044c");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(label1)
                        .addComponent(label2))
                    .addGap(33, 33, 33)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addComponent(passwordField)
                        .addComponent(loginField)
                        .addComponent(registerButton, GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                        .addComponent(loginButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap(141, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(71, 71, 71)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(loginField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label1))
                    .addGap(31, 31, 31)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label2))
                    .addGap(32, 32, 32)
                    .addComponent(loginButton)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                    .addComponent(registerButton)
                    .addGap(24, 24, 24))
        );
        pack();
        setLocationRelativeTo(getOwner());
    }// </editor-fold>//GEN-END:initComponents


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - d fd
    private JPasswordField passwordField;
    private JTextField loginField;
    private JButton registerButton;
    private JButton loginButton;
    private JLabel label1;
    private JLabel label2;
    // End of variables declaration//GEN-END:variables
    @Override
    public void keyTyped(KeyEvent e) {
        if(e.getKeyChar() == '\n'){
            enterActionPerformed();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
