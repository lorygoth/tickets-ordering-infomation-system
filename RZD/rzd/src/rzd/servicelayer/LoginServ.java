package rzd.servicelayer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import rzd.blogic.*;
import rzd.data.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginServ {
    private  ClientEntity                clientEntity;
    private  EntityManager               entityManager;
    private  List<CountryEntity>         countryEntities;
    private  List<RoutStationEntity>     routStationEntities;
    private  List<TicketEntity>          ticketEntities;

    public boolean isAdmin() {
        return clientEntity.isClientIsadmin();
    }

    public LoginServ() throws RemoteException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public boolean loginClient(String login, String password) {
        clientEntity = ClientLogic.loginClient(login, password, entityManager);
        return clientEntity != null;
    }

    public boolean registerUser(String login, String password) {
        clientEntity = ClientLogic.registerClient(login, password, entityManager);
        return clientEntity != null;
    }

    public List<String> getAllCountries() {
        countryEntities = CountryLogic.getAllCountries(entityManager);
        return countryEntities.stream().map(CountryEntity::getCountryName).collect(Collectors.toList());
    }

    public List<String> getLocalCountry(int countryIndex) {
       return getLocalByCountryIndex(countryIndex).stream().map(LocalEntity::getLocalName).collect(Collectors.toList());
    }

    public List<String> getStationLocal(int countryIndex, int localIndex) {
        return getStationByLocalIndex(countryIndex, localIndex).stream().map(StationEntity::getStationName)
                .collect(Collectors.toList());
    }

    private List<LocalEntity> getLocalByCountryIndex(int countryIndex) {
        CountryEntity countryEntity = countryEntities.get(countryIndex);
        return CountryLogic.getLocalByCountry(countryEntity);
    }

    private List<StationEntity> getStationByLocalIndex(int countryIndex, int localIndex) {
        List<LocalEntity> localEntities = getLocalByCountryIndex(countryIndex);
        return LocalLogic.getStationsByLocal(localEntities.get(localIndex));
    }

    public Object[][] search(int countryFromIndex, int countryToIndex, int localFromIndex, int localToIndex,
                             int stationFromIndex, int stationToIndex, Date date) {
        List<StationEntity> stationsFromEntities = getStationsFromSearch(countryFromIndex, localFromIndex, stationFromIndex);
        List<StationEntity> stationsToEntities = getStationsFromSearch(countryToIndex, localToIndex, stationToIndex);

        List<TripEntity> trips = TripLogic.getAvailableTrips(entityManager, stationsFromEntities, stationsToEntities, date);
        routStationEntities = new ArrayList<>();
        for (TripEntity trip : trips) {
            routStationEntities.addAll(RoutStationLogic.getRoutStationsByTrip(entityManager, trip));
        }

        Object[][] objects = new Object[routStationEntities.size()][6];
        for (int i = 0; i < routStationEntities.size(); i ++) {
            RoutStationEntity routStationEntity = routStationEntities.get(i);
            objects[i][0] =  routStationEntity.getTripByTripId().getTripId();
            objects[i][1] =  routStationEntity.getStationByStationFromId().getStationName();
            objects[i][2] =  routStationEntity.getStationByStationToId().getStationName();
            objects[i][3] =  routStationEntity.getTimeFrom().toString();
            objects[i][4] =  routStationEntity.getTimeTo().toString();
            objects[i][5] =  routStationEntity.getTripByTripId().getTrainTicketByTrainTicketId().availableTickets();
        }
       return objects;
    }

    private List<StationEntity> getStationsFromSearch(int countryIndex, int localIndex, int stationIndex) {
        CountryEntity countryEntity = countryEntities.get(countryIndex);
        List<StationEntity> stationsEntities = new ArrayList<>();

        if (localIndex >= 0) {
            LocalEntity localEntity = CountryLogic.getLocalByCountry(countryEntity).get(localIndex);
            if (stationIndex >= 0) {
                stationsEntities.add(LocalLogic.getStationsByLocal(localEntity).get(stationIndex));
            } else {
                stationsEntities = LocalLogic.getStationsByLocal(localEntity);
            }
        } else {
            for (LocalEntity localEntity : CountryLogic.getLocalByCountry(countryEntity)) {
                for (StationEntity stationEntity : LocalLogic.getStationsByLocal(localEntity)) {
                    stationsEntities.add(stationEntity);
                }
            }
        }
        return stationsEntities;
    }


    public Object[][] getTickets() {
        if (!isAdmin()) {
            ticketEntities = TicketLogic.getClientTickets(clientEntity, entityManager);
        } else  {
            ticketEntities = TicketLogic.getAllTickets(entityManager);
        }

        return getLinerViewTickets();
    }

    public Object[][] getLinerViewTickets() {
        Object[][] objects = new Object[ticketEntities.size()][2];
        for (int i = 0; i < ticketEntities.size(); i ++) {
            objects[i][0] = ticketEntities.get(i).getTicketId();
            objects[i][1] = ticketEntities.get(i).ticketStatusString();
        }
        return objects;
    }

    public void bookTicket(int routIndex) {
        TrainTicketEntity trainTicketEntity = routStationEntities.get(routIndex).getTripByTripId().getTrainTicketByTrainTicketId();
        TrainTicketLogic.bookTicket(trainTicketEntity, clientEntity, entityManager);
    }

    public void cancelTicket(int ticketIndex) {
        TicketLogic.changeTicketStatus(ticketEntities.get(ticketIndex), entityManager, 3);
    }

    public void okTicket(int ticketIndex) {
        TicketLogic.changeTicketStatus(ticketEntities.get(ticketIndex), entityManager, 2);
    }
}