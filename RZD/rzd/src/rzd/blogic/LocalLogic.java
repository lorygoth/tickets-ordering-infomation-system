package rzd.blogic;

import rzd.data.LocalEntity;
import rzd.data.StationEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class LocalLogic {
    public static List<StationEntity> getStationsByLocal (LocalEntity localEntity) {
        return (List<StationEntity>) localEntity.getStationsByLocalId();
    }
}
