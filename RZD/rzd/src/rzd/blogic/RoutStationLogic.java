package rzd.blogic;

import rzd.data.RoutStationEntity;
import rzd.data.TripEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class RoutStationLogic {
    public static List<RoutStationEntity> getRoutStationsByTrip(EntityManager entityManager, TripEntity tripEntity) {
        return entityManager.createNamedQuery("RoutStation.findByTripID").setParameter("trip", tripEntity).getResultList();
    }
}
