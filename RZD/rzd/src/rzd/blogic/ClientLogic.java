package rzd.blogic;

import rzd.data.ClientEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class ClientLogic {
    public static ClientEntity loginClient(String login, String password, EntityManager entityManager) {
        List<ClientEntity> ClientEntityList = entityManager.createNamedQuery("Client.findByLoginPassword")
                                              .setParameter("login", login).setParameter("password", password)
                                              .getResultList();
        if (ClientEntityList.size() > 0) {
            return ClientEntityList.get(0);
        } else {
            return null;
        }
    }
    public static ClientEntity registerClient(String login, String password, EntityManager entityManager) {
        List<ClientEntity> ClientEntityList = entityManager.createNamedQuery("Client.findByLogin").setParameter("login", login).getResultList();
        if (ClientEntityList.size() > 0) {
            return null;
        }

        entityManager.getTransaction().begin();
        ClientEntity ClientEntity = new ClientEntity();
        ClientEntity.setClientLogin(login);
        ClientEntity.setClientPassword(password);
        ClientEntity.setClientIsadmin(false);
        entityManager.persist(ClientEntity);
        entityManager.getTransaction().commit();
        ClientEntityList = entityManager.createNamedQuery("Client.findByLogin").setParameter("login", login).getResultList();
        return ClientEntityList.get(0);
    }
}
