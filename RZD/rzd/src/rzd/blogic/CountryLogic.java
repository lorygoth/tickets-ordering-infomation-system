package rzd.blogic;

import rzd.data.CountryEntity;
import rzd.data.LocalEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class CountryLogic {
    public static List<CountryEntity> getAllCountries(EntityManager entityManager) {
        return entityManager.createNamedQuery("Country.findAll").getResultList();
    }

    public static List<LocalEntity> getLocalByCountry(CountryEntity countryEntity) {
        return (List<LocalEntity>) countryEntity.getLocalsByCountryId();
    }
}
