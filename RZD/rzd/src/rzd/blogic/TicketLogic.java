package rzd.blogic;

import rzd.data.ClientEntity;
import rzd.data.TicketEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class TicketLogic {
    public static List<TicketEntity> getClientTickets(ClientEntity clientEntity, EntityManager entityManager) {
        return entityManager.createNamedQuery("Ticket.findByClient").setParameter("client", clientEntity).getResultList();
    }

    public static List<TicketEntity> getAllTickets(EntityManager entityManager) {
        return entityManager.createNamedQuery("Ticket.findAll").getResultList();
    }

    public static void changeTicketStatus(TicketEntity ticketEntity, EntityManager entityManager, int status) {
        entityManager.getTransaction().begin();
        ticketEntity.setTicketStatus(status);
        entityManager.persist(ticketEntity);
        entityManager.getTransaction().commit();
    }
}