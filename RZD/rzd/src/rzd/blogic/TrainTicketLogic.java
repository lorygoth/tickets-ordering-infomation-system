package rzd.blogic;

import rzd.data.ClientEntity;
import rzd.data.TicketEntity;
import rzd.data.TrainTicketEntity;

import javax.persistence.EntityManager;

public class TrainTicketLogic {
    public static void bookTicket(TrainTicketEntity trainTicketEntity, ClientEntity clientEntity, EntityManager entityManager) {
        entityManager.getTransaction().begin();
        trainTicketEntity.setTrainTicketBooked(trainTicketEntity.getTrainTicketBooked() + 1);
        TicketEntity ticketEntity = new TicketEntity();
        ticketEntity.setTicketStatus(1);
        ticketEntity.setTrainTicketByTrainTicketId(trainTicketEntity);
        ticketEntity.setClientByClientId(clientEntity);
        entityManager.persist(trainTicketEntity);
        entityManager.persist(ticketEntity);
        entityManager.getTransaction().commit();
    }
}
