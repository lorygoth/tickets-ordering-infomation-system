package rzd.blogic;

import rzd.data.StationEntity;
import rzd.data.TripEntity;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TripLogic {
    public static List<TripEntity> getAvailableTrips(EntityManager entityManager, List<StationEntity> from,
                                                     List<StationEntity> to, Date date) {
        List<TripEntity> allTrips = new ArrayList<>();
        for (StationEntity stationFrom : from) {
            List<TripEntity> tripFrom = entityManager.createNamedQuery("Trip.findByAvailable")
                                                     .setParameter("from", stationFrom)
                                                     .setParameter("day", date.getDay())
                                                     .setParameter("month", date.getMonth())
                                                     .setParameter("year", date.getYear())
                                                     .getResultList();
            for (TripEntity trip : tripFrom) {
                for (StationEntity stationTo : to) {
                    allTrips.addAll(entityManager.createNamedQuery("Trip.findByIDAndTo")
                                                 .setParameter("trip", trip)
                                                 .setParameter("to", stationTo)
                                                 .getResultList());
                }
            }
        }
        return allTrips;
    }
}