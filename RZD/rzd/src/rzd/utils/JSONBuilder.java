package rzd.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import rzd.data.TrainTicketEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.rmi.RemoteException;

public class JSONBuilder {
    private EntityManager entityManager;

    public JSONBuilder() throws RemoteException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        entityManager = entityManagerFactory.createEntityManager();
    }
    public JSONObject prepareAndGetJSON (int id) {
        TrainTicketEntity trainTicketEntity = entityManager.find(TrainTicketEntity.class, id);
        JSONObject mainObj = new JSONObject();

        JSONArray ticketLine = new JSONArray();
        ticketLine.add(trainTicketEntity.getTrainTicketId());
        ticketLine.add(trainTicketEntity.getTrainTicketAmount());
        ticketLine.add(trainTicketEntity.getTrainTicketBought());
        ticketLine.add(trainTicketEntity.getTrainTicketBooked());

        mainObj.put("tickets", ticketLine);
        return  mainObj;
    }
}
