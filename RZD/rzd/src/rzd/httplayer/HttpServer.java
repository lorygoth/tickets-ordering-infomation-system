package rzd.httplayer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
//http://localhost:9999/tripTicketID
public class HttpServer implements Runnable {
    private static final int DEFAULT_PORT = 9999;
    @Override
    public void run() {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(DEFAULT_PORT);
            System.out.println(new StringBuilder().append("Server started on port: ").append(serverSocket.getLocalPort())
                               .append("\n").toString());
        } catch (IOException e) {
            System.out.println(new StringBuilder().append("Port ").append(DEFAULT_PORT).append(" is blocked.").toString());
            return;
        } 
        while (true) {
            try {
                Socket clientSocket = serverSocket.accept(); 
                ClientSession session = new ClientSession(clientSocket);
                new Thread(session).start();
            } catch (IOException e) {
                System.out.println("Failed to establish connection.");
                System.out.println(e.getMessage());
                System.exit(-1);
            }
        }
    }
}