package rzd.httplayer;

import rzd.utils.JSONBuilder;
import org.json.simple.JSONObject;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Date;

public class ClientSession implements Runnable {
    private                  Socket          socket;
    private                  InputStream     in;
    private                  OutputStream    out;

    private  static  final   int             BAD_CODE               = 404;
    private  static  final   int             GOOD_CODE              = 200;
    private  static  final   String          WINDOWS_ENCODING       = "windows-1251";


    @Override
    public void run() {
        try {
            String header = readHeader();
            System.out.println(header + "\n"); 
            int tripTicketId = getTripTicketIDFromHeader(header);
            System.out.println(new StringBuilder().append("trip_ticket_id: ").append(tripTicketId).append("\n").toString());
            int code = BAD_CODE;
            JSONBuilder jsonBuilder = new JSONBuilder();
            JSONObject jsonObject = jsonBuilder.prepareAndGetJSON(tripTicketId);
            if (jsonObject != null) {
                code = send(jsonObject);
            } else {
                send(null);
            }
            System.out.println("Result code: " + code + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ClientSession(Socket socket) throws IOException {
        this.socket = socket;
        initialize();
    }

    private void initialize() throws IOException { 
        in = socket.getInputStream();
        out = socket.getOutputStream();
    }

    private String readHeader() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder builder = new StringBuilder();
        String ln;
        while (true) {
            ln = reader.readLine();
            if (ln == null || ln.isEmpty()) {
                break;
            }
            builder.append(ln).append(System.getProperty("line.separator"));
        }
        return builder.toString();
    }

    private int getTripTicketIDFromHeader(String header) {
        int from = header.indexOf(" ") + 1;
        int to = header.indexOf(" ", from);
        String IDString = header.substring(from + 1, to);
        int paramIndex = IDString.indexOf("/");
        if (paramIndex != -1) {
            IDString = IDString.substring(0, paramIndex);
        }
        try {
            return Integer.valueOf(IDString);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private int send(JSONObject jsonObject) throws IOException {
        if(jsonObject == null) {
            return BAD_CODE;
        }
        String header = getHeader(GOOD_CODE); // "UTF-8"
        PrintStream answer = new PrintStream(out, true, WINDOWS_ENCODING);
        answer.print(header);
        byte[] b = jsonObject.toJSONString().getBytes(Charset.forName(WINDOWS_ENCODING));
        out.write(b, 0, b.length);
        return GOOD_CODE;
    }

    private String getHeader(int code) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("HTTP/1.1 ").append(code).append(" ").append(getAnswer(code)).append("\n");
        buffer.append("Date: ").append(new Date().toGMTString()).append("\n");
        buffer.append("Accept-Ranges: none\n");
        buffer.append("\n");
        return buffer.toString();
    }

    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return "OK";
            case 404:
                return "Not Found";
            default:
                return "Internal Server Error";
        }
    }
}