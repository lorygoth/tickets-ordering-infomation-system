package rzd.data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "rout_station", schema = "public", catalog = "rzd")
@NamedQueries({
        @NamedQuery(name = "RoutStation.findByTripID",
                query = "FROM RoutStationEntity WHERE tripByTripId = :trip")
})
public class RoutStationEntity {
    private  Integer         routStationId;
    private  Timestamp       timeFrom;
    private  Timestamp       timeTo;
    private  StationEntity   stationByStationFromId;
    private  StationEntity   stationByStationToId;
    private  TripEntity      tripByTripId;

    @Id
    @Column(name = "rout_station_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getRoutStationId() {
        return routStationId;
    }

    public void setRoutStationId(Integer routStationId) {
        this.routStationId = routStationId;
    }

    @Basic
    @Column(name = "time_from", nullable = true, insertable = true, updatable = true)
    public Timestamp getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(Timestamp timeFrom) {
        this.timeFrom = timeFrom;
    }

    @Basic
    @Column(name = "time_to", nullable = true, insertable = true, updatable = true)
    public Timestamp getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(Timestamp timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoutStationEntity that = (RoutStationEntity) o;

        if (routStationId != null ? !routStationId.equals(that.routStationId) : that.routStationId != null)
            return false;
        if (timeFrom != null ? !timeFrom.equals(that.timeFrom) : that.timeFrom != null) return false;
        if (timeTo != null ? !timeTo.equals(that.timeTo) : that.timeTo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = routStationId != null ? routStationId.hashCode() : 0;
        result = 31 * result + (timeFrom != null ? timeFrom.hashCode() : 0);
        result = 31 * result + (timeTo != null ? timeTo.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "station_from_id", referencedColumnName = "station_id")
    public StationEntity getStationByStationFromId() {
        return stationByStationFromId;
    }

    public void setStationByStationFromId(StationEntity stationByStationFromId) {
        this.stationByStationFromId = stationByStationFromId;
    }

    @ManyToOne
    @JoinColumn(name = "station_to_id", referencedColumnName = "station_id")
    public StationEntity getStationByStationToId() {
        return stationByStationToId;
    }

    public void setStationByStationToId(StationEntity stationByStationToId) {
        this.stationByStationToId = stationByStationToId;
    }

    @ManyToOne
    @JoinColumn(name = "trip_id", referencedColumnName = "trip_id")
    public TripEntity getTripByTripId() {
        return tripByTripId;
    }

    public void setTripByTripId(TripEntity tripByTripId) {
        this.tripByTripId = tripByTripId;
    }
}