package rzd.data;

import javax.persistence.*;

@Entity
@Table(name = "train_ticket", schema = "public", catalog = "rzd")
public class TrainTicketEntity {
    private  Integer     trainTicketId;
    private  Integer     trainTicketAmount;
    private  Integer     trainTicketBooked;
    private  Integer     trainTicketBought;

    @Id
    @Column(name = "train_ticket_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getTrainTicketId() {
        return trainTicketId;
    }

    public void setTrainTicketId(Integer trainTicketId) {
        this.trainTicketId = trainTicketId;
    }

    @Basic
    @Column(name = "train_ticket_amount", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getTrainTicketAmount() {
        return trainTicketAmount;
    }

    public void setTrainTicketAmount(Integer trainTicketAmount) {
        this.trainTicketAmount = trainTicketAmount;
    }

    @Basic
    @Column(name = "train_ticket_booked", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getTrainTicketBooked() {
        return trainTicketBooked;
    }

    public void setTrainTicketBooked(Integer trainTicketBooked) {
        this.trainTicketBooked = trainTicketBooked;
    }

    @Basic
    @Column(name = "train_ticket_bought", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getTrainTicketBought() {
        return trainTicketBought;
    }

    public void setTrainTicketBought(Integer trainTicketBought) {
        this.trainTicketBought = trainTicketBought;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrainTicketEntity that = (TrainTicketEntity) o;

        if (trainTicketId != null ? !trainTicketId.equals(that.trainTicketId) : that.trainTicketId != null)
            return false;
        if (trainTicketAmount != null ? !trainTicketAmount.equals(that.trainTicketAmount) : that.trainTicketAmount != null)
            return false;
        if (trainTicketBooked != null ? !trainTicketBooked.equals(that.trainTicketBooked) : that.trainTicketBooked != null)
            return false;
        if (trainTicketBought != null ? !trainTicketBought.equals(that.trainTicketBought) : that.trainTicketBought != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = trainTicketId != null ? trainTicketId.hashCode() : 0;
        result = 31 * result + (trainTicketAmount != null ? trainTicketAmount.hashCode() : 0);
        result = 31 * result + (trainTicketBooked != null ? trainTicketBooked.hashCode() : 0);
        result = 31 * result + (trainTicketBought != null ? trainTicketBought.hashCode() : 0);
        return result;
    }
    
    public int availableTickets () {
        return (trainTicketAmount - trainTicketBooked - trainTicketBought);
    }
}