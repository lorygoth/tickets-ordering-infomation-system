package rzd.data;

import javax.persistence.*;

@Entity
@Table(name = "client", schema = "public", catalog = "rzd")
@NamedQueries({
        @NamedQuery(name = "Client.findByLoginPassword", query = "FROM ClientEntity WHERE clientLogin = :login " +
                                                                 "AND clientPassword = :password"),
        @NamedQuery(name = "Client.findByLogin",         query = "FROM ClientEntity WHERE clientLogin = :login ")
})
public class ClientEntity {
    private  Integer clientId;
    private  String  clientLogin;
    private  String  clientPassword;
    private  boolean clientIsadmin;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "client_login", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getClientLogin() {
        return clientLogin;
    }

    public void setClientLogin(String clientLogin) {
        this.clientLogin = clientLogin;
    }

    @Basic
    @Column(name = "client_password", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientEntity that = (ClientEntity) o;

        if (clientId != null ? !clientId.equals(that.clientId) : that.clientId != null) return false;
        if (clientLogin != null ? !clientLogin.equals(that.clientLogin) : that.clientLogin != null) return false;
        return !(clientPassword != null ? !clientPassword.equals(that.clientPassword) : that.clientPassword != null);

    }

    @Override
    public int hashCode() {
        Integer result = clientId != null ? clientId.hashCode() : 0;
        result = 31 * result + (clientLogin != null ? clientLogin.hashCode() : 0);
        result = 31 * result + (clientPassword != null ? clientPassword.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "client_isadmin", nullable = true, insertable = true, updatable = true)
    public boolean isClientIsadmin() {
        return clientIsadmin;
    }

    public void setClientIsadmin(boolean clientIsadmin) {
        this.clientIsadmin = clientIsadmin;
    }
}
