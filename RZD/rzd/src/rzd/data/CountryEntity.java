package rzd.data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@NamedQueries({
        @NamedQuery(name = "Country.findAll", query = "FROM CountryEntity")
})
@Table(name = "country", schema = "public", catalog = "rzd")
public class CountryEntity {
    private  Integer                     countryId;
    private  String                      countryName;
    private  Collection<LocalEntity>     localsByCountryId;

    @Id
    @Column(name = "country_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    @Basic
    @Column(name = "country_name", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryEntity that = (CountryEntity) o;

        if (countryId != null ? !countryId.equals(that.countryId) : that.countryId != null) return false;
        if (countryName != null ? !countryName.equals(that.countryName) : that.countryName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = countryId != null ? countryId.hashCode() : 0;
        result = 31 * result + (countryName != null ? countryName.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "countryByCountryId")
    public Collection<LocalEntity> getLocalsByCountryId() {
        return localsByCountryId;
    }

    public void setLocalsByCountryId(Collection<LocalEntity> localsByCountryId) {
        this.localsByCountryId = localsByCountryId;
    }
}