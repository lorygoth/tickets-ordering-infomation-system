package rzd.data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "local", schema = "public", catalog = "rzd")
public class LocalEntity {
    private  Integer                     localId;
    private  String                      localName;
    private  CountryEntity               countryByCountryId;
    private  Collection<StationEntity>   stationsByLocalId;

    @Id
    @Column(name = "local_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    @Basic
    @Column(name = "local_name", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocalEntity that = (LocalEntity) o;

        if (localId != null ? !localId.equals(that.localId) : that.localId != null) return false;
        if (localName != null ? !localName.equals(that.localName) : that.localName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = localId != null ? localId.hashCode() : 0;
        result = 31 * result + (localName != null ? localName.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName = "country_id")
    public CountryEntity getCountryByCountryId() {
        return countryByCountryId;
    }

    public void setCountryByCountryId(CountryEntity countryByCountryId) {
        this.countryByCountryId = countryByCountryId;
    }

    @OneToMany(mappedBy = "localByLocalId")
    public Collection<StationEntity> getStationsByLocalId() {
        return stationsByLocalId;
    }

    public void setStationsByLocalId(Collection<StationEntity> stationsByLocalId) {
        this.stationsByLocalId = stationsByLocalId;
    }
}