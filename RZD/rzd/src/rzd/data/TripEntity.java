package rzd.data;

import javax.persistence.*;

@Entity
@Table(name = "trip", schema = "public", catalog = "rzd")
@NamedQueries({
        @NamedQuery(name = "Trip.findByAvailable",
                   query = "SELECT te FROM RoutStationEntity rse JOIN  rse.tripByTripId te " +
                           "JOIN te.trainTicketByTrainTicketId tte " +
                           "WHERE (tte.trainTicketAmount - tte.trainTicketBooked - tte.trainTicketBought) > 0 " +
                           "AND (rse.stationByStationFromId) = :from AND day(rse.timeFrom) = :day " +
                           "AND month(rse.timeFrom) = :month AND year(rse.timeFrom) = :year"),
        @NamedQuery(name = "Trip.findByIDAndTo",
                    query = "SELECT te FROM RoutStationEntity rse JOIN  rse.tripByTripId te WHERE te = :trip " +
                            "AND rse.stationByStationToId = :to")
})
public class TripEntity {
    private Integer tripId;
    private TrainTicketEntity trainTicketByTrainTicketId;

    @Id
    @Column(name = "trip_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TripEntity that = (TripEntity) o;

        if (tripId != null ? !tripId.equals(that.tripId) : that.tripId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return tripId != null ? tripId.hashCode() : 0;
    }

    @ManyToOne
    @JoinColumn(name = "train_ticket_id", referencedColumnName = "train_ticket_id")
    public TrainTicketEntity getTrainTicketByTrainTicketId() {
        return trainTicketByTrainTicketId;
    }

    public void setTrainTicketByTrainTicketId(TrainTicketEntity trainTicketByTrainTicketId) {
        this.trainTicketByTrainTicketId = trainTicketByTrainTicketId;
    }
}