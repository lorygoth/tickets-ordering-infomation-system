package rzd.data;

import javax.persistence.*;

@Entity
@Table(name = "ticket", schema = "public", catalog = "rzd")
@NamedQueries({
        @NamedQuery(name = "Ticket.findByClient",
                    query = "FROM TicketEntity WHERE clientByClientId = :client ORDER BY ticketId"),
        @NamedQuery(name = "Ticket.findAll",
                    query = "FROM TicketEntity ORDER BY ticketId")
})
public class TicketEntity {
    private  Integer             ticketId;
    private  Integer             ticketStatus;
    private  TrainTicketEntity   trainTicketByTrainTicketId;
    private  ClientEntity        clientByClientId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    @Basic
    @Column(name = "ticket_status", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(Integer ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketEntity that = (TicketEntity) o;

        if (ticketId != null ? !ticketId.equals(that.ticketId) : that.ticketId != null) return false;
        if (ticketStatus != null ? !ticketStatus.equals(that.ticketStatus) : that.ticketStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ticketId != null ? ticketId.hashCode() : 0;
        result = 31 * result + (ticketStatus != null ? ticketStatus.hashCode() : 0);
        return result;
    }

    @ManyToOne
         @JoinColumn(name = "train_ticket_id", referencedColumnName = "train_ticket_id")
         public TrainTicketEntity getTrainTicketByTrainTicketId() {
        return trainTicketByTrainTicketId;
    }

    public void setTrainTicketByTrainTicketId(TrainTicketEntity trainTicketByTrainTicketId) {
        this.trainTicketByTrainTicketId = trainTicketByTrainTicketId;
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "client_id")
    public ClientEntity getClientByClientId() {
        return clientByClientId;
    }

    public void setClientByClientId(ClientEntity clientByClientId) {
        this.clientByClientId = clientByClientId;
    }

    public String ticketStatusString () {
        switch (ticketStatus){
            case 1:
                return "booked";
            case 2:
                return "approve";
            case 3:
                return "canceled";
        }
        return "error";
    }
}