package rzd.data;

import javax.persistence.*;

@Entity
@Table(name = "station", schema = "public", catalog = "rzd")
public class StationEntity {
    private Integer      stationId;
    private String       stationName;
    private LocalEntity  localByLocalId;

    @Id
    @Column(name = "station_id", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    @Basic
    @Column(name = "station_name", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StationEntity that = (StationEntity) o;

        if (stationId != null ? !stationId.equals(that.stationId) : that.stationId != null) return false;
        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = stationId != null ? stationId.hashCode() : 0;
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "local_id", referencedColumnName = "local_id")
    public LocalEntity getLocalByLocalId() {
        return localByLocalId;
    }

    public void setLocalByLocalId(LocalEntity localByLocalId) {
        this.localByLocalId = localByLocalId;
    }
}